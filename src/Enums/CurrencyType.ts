export enum CurrencyType {
    Dolar = "$",
    Shekel = "₪",
    Euro = "€"
}