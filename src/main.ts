import App from './app';
import DestinationsController from './Controllers/destinations.controller';
import TranslationsController from './Controllers/translations.controller';
 
const app = new App(
  [
    new TranslationsController(),
    new DestinationsController()
  ],
  5000,
);
 
app.listen();