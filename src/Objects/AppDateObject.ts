import { CurrencyType } from "../Enums/CurrencyType";
import { Month } from "../Enums/Month";

export class AppDataObject {
  public lastPulling: Date;
  constructor(lastPulling: Date) {
    this.lastPulling = lastPulling;
  }
}