import * as express from 'express';
import { Translation } from '../Objects/Translation';
import { collections } from '../services/database.service';
import BaseController from './base.controller.ts';

class TranslationsController extends BaseController {
  public path = '/translations';

  constructor() {
    super();
    this.intializeRoutes();
  }

  public intializeRoutes(): void {
    this.router.get(this.path, this.getTranslations);
    this.router.post(this.path, this.addTranslations);
  }

  addTranslations = async (request: express.Request, response: express.Response) => {
    try {
        const newTranslations = request.body;
        const result = await collections.translations!.insertMany(newTranslations);

        result? response.status(201).send(`Successfully created a new translations ${result}`)
          : response.status(500).send("Failed to create a new translation.");

    } catch (error: any) {
      response.status(400).send(error.message);
    }
  }

  getTranslations = async (request: express.Request, response: express.Response) => {
    try {
        // Call find with an empty filter object, meaning it returns all documents in the collection. Saves as Game array to take advantage of types
        const translations = await collections.translations!.find({}).toArray();
        response.status(200).send(translations);
    } catch (error: any) {
      response.status(500).send(error.message);
    }
  }
}

export default TranslationsController;