import * as express from 'express';
import { AppDataManager } from '../DataManagement/AppDataManager';
import { collections } from '../services/database.service';
import { DestinationsService } from '../services/destinations.service';
import BaseController from './base.controller.ts';

class DestinationsController extends BaseController {
  public path = '/destinations';

  constructor() {
    super();
    this.intializeRoutes();
  }

  public intializeRoutes(): void {
    this.router.get(this.path, this.getDestinations);
    this.router.post(this.path, this.addDestinations);
    this.router.delete(this.path, this.deleteDestinations);
  }

  deleteDestinations = async (request: express.Request, response: express.Response) => {
    try {
      await collections.destinations!.deleteMany({});
      response.status(200).send(`All destinations were deleted succesfully`);
    } catch (error: any) {
      console.error(error.message);
      response.status(400).send(error.message);
    }
  }

  addDestinations = async (request: express.Request, response: express.Response) => {
    try {
      const newDestinations = request.body;
      const result = await collections.destinations!.insertMany(newDestinations);
      AppDataManager.UpdateLastPulling();
      result ? response.status(201).send(`Successfully created new destinations ${result}`)
        : response.status(500).send("Failed to create new destinations.");

    } catch (error: any) {
      response.status(400).send(error.message);
    }
  }

  getDestinations = async (request: express.Request, response: express.Response) => {
    try {
      if (!await AppDataManager.IsDestinationsRelevent()) {
        await collections.destinations!.deleteMany({});
        const destinations = await DestinationsService.getAllDestinations();
        await collections.destinations!.insertMany(destinations);
        AppDataManager.UpdateLastPulling();
        response.status(200).send(destinations);
      }
      else {
        // Call find with an empty filter object, meaning it returns all documents in the collection.
        const destinations = await collections.destinations!.find({}).toArray();
        response.status(200).send(destinations);
      }

    } catch (error: any) {
      response.status(500).send(error.message);
    }
  }
}

export default DestinationsController;