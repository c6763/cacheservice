import { AppDataObject } from '../Objects/AppDateObject';
import { collections } from '../services/database.service';

export class AppDataManager {
    public static async IsDestinationsRelevent(): Promise<boolean> {
        const day = 1000 * 60 * 60 * 24;
        const dayago = Date.now() - day;
        const lastPullingDate = await this.GetLastPullingDate();
        if (!lastPullingDate) return false;
        return lastPullingDate!.getTime() > dayago;
    }

    public static async UpdateLastPulling(): Promise<void> {
        try {
            let appData = new AppDataObject(new Date(Date.now()));
            await collections.lastPulling!.deleteMany({});
            await collections.lastPulling!.insertMany([appData]);
        } catch (error) {
            console.log(error);
        }
    }

    public static async GetAppDataObject(): Promise<AppDataObject> {
        return (await collections.lastPulling!.find({}).toArray())[0];
    }

    private static async GetLastPullingDate(): Promise<Date | undefined> {
        try {
            let appDataObjectList = await collections.lastPulling!.find({}).toArray();
            if(appDataObjectList.length == 0) {
                return undefined;
            }
            return appDataObjectList[0].lastPulling;
        } catch (error) {
            return undefined;
        }
    }
}