import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";
import { Translation } from "../Objects/Translation";
import { Destination } from "../Objects/Destination";
import { AppDataObject } from "../Objects/AppDateObject";

const DB_CONN_STRING = "mongodb://localhost:27017";
const DB_NAME = "CheapFlightsCache"; 
const TRANSLATIONS_COLLECTION_NAME = "Translations";
const DESTINATIONS_COLLECTION_NAME = "Destinations";
const LAST_PULLING_COLLECTION_NAME = "LastPulling";

export const collections: { translations?: mongoDB.Collection<Translation>, destinations?: mongoDB.Collection<Destination>, lastPulling?: mongoDB.Collection<AppDataObject>} = {};

export async function connectToDatabase() {
    // Pulls in the .env file so it can be accessed from process.env. No path as .env is in root, the default location
    dotenv.config();

    // Create a new MongoDB client with the connection string from .env
    const client = new mongoDB.MongoClient(DB_CONN_STRING);

    // Connect to the cluster
    await client.connect();

    // Connect to the database with the name specified in .env

    const db = client.db(DB_NAME);
    
    // Apply schema validation to the collection
    //await applySchemaValidation(db);

    // Connect to the collection with the specific name from .env, found in the database previously specified
    const translationsCollection = db.collection<Translation>(TRANSLATIONS_COLLECTION_NAME);
    const destinationsCollection = db.collection<Destination>(DESTINATIONS_COLLECTION_NAME);
    const lastPullingCollection = db.collection<AppDataObject>(LAST_PULLING_COLLECTION_NAME);

    // Persist the connection to the Translations collection
    collections.translations = translationsCollection;
    collections.destinations = destinationsCollection;
    collections.lastPulling = lastPullingCollection;

    console.log(
        `Successfully connected to database: ${db.databaseName} and collection: ${translationsCollection.collectionName} and collection: ${destinationsCollection.collectionName}`,
    );
}
