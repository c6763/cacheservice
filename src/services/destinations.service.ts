import axios from "axios";
import { Destination } from "../Objects/Destination";

export class DestinationsService {
    private static destinationsAPI = "http://localhost:5001/destinations";
    constructor() {
    }

    public static async getAllDestinations(): Promise<Destination[]> {
        return (await axios.get(this.destinationsAPI)).data as Destination[];
    }
}